package src.model;

import java.awt.Rectangle;


/**
 * 
 * @author Federico Galdenzi
 * This interface provide several methods to move the ball updating the X and Y coordinates in according to the ball's speed.
 * Also you can get the Rectangle object from position and size of the ball, get property of the ball and who has score.
 */

public interface Ball {
    /**
     * this method update the X,Y coordinates in according to the speed of the ball
     * also check if ball go over the field's bound and set who has scored.
     */
    void move();
    /**
     * @return if someone of the player has score.
     */
    boolean hasScore();
    /**
     * @return who has scored.
     */
    int getNewPointTo();
    /**
     * reset who has score and if someone has score.
     */
    void resetHasScore();
    /**
     * 
     * @return the Rectangle class in according to the X,Y position and size of the ball.
     */
    Rectangle getShape();
    /**
     * @return the BallProprierty class of the ball.
     */
    BallProprierty getProprierty();
}
