package src.model;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import src.observerpattern.ExpiringBonusTimerObserver;
import src.observerpattern.GraphicBonusShowObserver;
import src.utilities.BonusType;
import src.utilities.Utility;

/**
 * 
 * @author Federico Galdenzi This class provide several methods that allows you
 *         to generate new bonus. It has a list of bonus applied to the ball,
 *         when the hasCollision() in CollisionBonus and returns true the bonus
 *         will be added to the list. It has a list of bonus generated in the
 *         playing field. It provides methods to manage the list of bonus
 *         applied to the ball.
 */
public class BonusManager implements ExpiringBonusTimerObserver {
    private final List<Bonus> generatedBonusList;
    private final List<BonusTimer> appliedBonusList;
    private List<BonusGenerationTimer> bonusGenerationTimers;
    private GraphicBonusShowObserver graphicObs;
    private final int numberOfBonus;
    private Rectangle field;
    private int x;
    private int y;

    /**
     * This is the constructor of the class.
     * 
     * @param numberOfBonus
     *            the maximum number of bonus you want to generate in the playing
     *            field.
     * @param bound
     *            the Boundary of the playing field.
     * @param obs
     *            this is the observer of the view
     */
    public BonusManager(final int numberOfBonus, final Boundary bound, final GraphicBonusShowObserver obs) {
        generatedBonusList = new ArrayList<>();
        appliedBonusList = new ArrayList<>();
        bonusGenerationTimers = new ArrayList<>();
        this.setField(bound);
        this.numberOfBonus = numberOfBonus;
        this.graphicObs = obs;
    }

    /**
     * 
     * @return the list of the bonus generated.
     */
    public List<Bonus> getGeneratedBonusList() {
        return Collections.unmodifiableList(this.generatedBonusList);
    }

    private Optional<BonusTimer> getBonusTimerFromBonus(final Ball ball, final Bonus bonus) {
        Optional<BonusTimer> bonusTimer = Optional.empty();
        if (bonus.getType() == BonusType.SPEED) {
            bonusTimer = Optional
                    .of(new SpeedBonus(ball, this, GameConstant.SPEED_BONUS_TIMER_INTERVAL * Utility.MSEC_CONST,
                            GameConstant.SPEED_BONUS_TIMER_DURATION * Utility.MSEC_CONST));
        } else if (bonus.getType() == BonusType.BLINK) {
            bonusTimer = Optional.of(new BlinkingBonus(ball, this,
                    (int) Math.round(GameConstant.BLINKING_BONUS_TIMER_INTERVAL * Utility.MSEC_CONST),
                    GameConstant.BLINKING_BONUS_TIMER_DURATION * Utility.MSEC_CONST));
        } else if (bonus.getType() == BonusType.SIZE) {
            bonusTimer = Optional.of(new SizeBonus(ball, this,
                    (int) Math.round(GameConstant.SIZE_BONUS_TIMER_INTERVAL * Utility.MSEC_CONST),
                    GameConstant.SIZE_BONUS_TIMER_DURATION * Utility.MSEC_CONST));
        }
        return bonusTimer;
    }

    private boolean isBonusTimerAppliable(final BonusTimer bonusTimer) {
        boolean appliable = false;

        if (bonusTimer.getClass().isAssignableFrom(SpeedBonus.class)) {
            if ((this.appliedBonusList.stream().filter(x -> x.getClass().isAssignableFrom(SpeedBonus.class))
                    .count() < 2)) {
                appliable = true;
            } /*
               * else { System.out.println("Speed bonus limit number reached"); }
               */
        } else if (bonusTimer.getClass().isAssignableFrom(BlinkingBonus.class)) {
            if (this.appliedBonusList.stream().noneMatch(x -> x.getClass().isAssignableFrom(BlinkingBonus.class))) {
                appliable = true;
            } /*
               * else { System.out.println("blinking bonus already applied"); }
               */
        } else if (bonusTimer.getClass().isAssignableFrom(SizeBonus.class)) {
            if (this.appliedBonusList.stream().noneMatch(x -> x.getClass().isAssignableFrom(SizeBonus.class))) {
                appliable = true;
            } /*
               * else { System.out.println("sizebonus already applied"); }
               */
        }
        return appliable;
    }
    /**
     * This method get the instance of the bonus hitted and then it will add it to the appliedBonusList
     * also it would apply the bonusStrategy to the ball.
     * @param ball the ball
     * @param bonus the bonus hitted
     */
    public void addAppliedBonus(final Ball ball, final Bonus bonus) {
        Optional<BonusTimer> bonusTimer = getBonusTimerFromBonus(ball, bonus);
        if (bonusTimer.isPresent()) {
            if (isBonusTimerAppliable(bonusTimer.get())) {
                this.appliedBonusList.add(bonusTimer.get());
                this.deleteGeneratedBonus(bonus.getPosition());
                this.startNewBonusGeneration();
                bonusTimer.get().applyStrategy();
            }
        }
    }

    private void setField(final Boundary bound) {
        int quarter = bound.getXBoundLaneRight() / 4;
        int middle = bound.getXBoundLaneRight() / 2;
        int start = middle - quarter;
        int end = middle + quarter;

        this.field = new Rectangle(start, bound.getYBoundLaneUp(), end - start,
                bound.getYBoundLaneDown() - 2 * GameConstant.BALL_DIAMETER);
    }

    /**
     * this method it is used to generate the bonus missing in the playing field.
     */
    public void generateExpectedMaximumBonus() {
        int i = this.generatedBonusList.size();
        while (i < this.numberOfBonus) {
            if (generateBonus()) {
                i++;
            }
        }
    }

    /**
     * this method generate one bonus in the playing field.
     * 
     * @return a boolean that it is true if the bonus is generated, false if it
     *         isn't generated
     */
    public boolean generateBonus() {
        Random rnd = new Random();
        x = rnd.nextInt((int) this.field.getMaxX() - (int) this.field.getMinX()) + (int) this.field.getMinX();
        y = rnd.nextInt((int) this.field.getMaxY());

        if (this.generatedBonusList.size() == this.numberOfBonus || generatedBonusList.stream()
                .anyMatch(bonus -> comparePosition(bonus.getPosition(), new Point(x, y)))) {
            return false;
        } else {
            Bonus b;
            b = new BonusImpl(new Point(x, y), BonusType.getRandomBonusType(), GameConstant.BONUS_DIAMETER);
            generatedBonusList.add(b);
            return true;
        }
    }

    /**
     * 
     * @return the last bonus created.
     */
    public final Bonus getLastBonusCreated() {
        if (this.generatedBonusList.size() > 0) {
            return this.generatedBonusList.get(this.generatedBonusList.size() - 1);
        }
        return null;

    }

    private boolean comparePosition(final Point a, final Point b) {
        Rectangle e1, e2;
        e1 = new Rectangle(a.x, a.y, GameConstant.BONUS_DIAMETER, GameConstant.BONUS_DIAMETER);
        e2 = new Rectangle(b.x, b.y, GameConstant.BONUS_DIAMETER, GameConstant.BONUS_DIAMETER);
        return e1.intersects(e2);
    }

    /**
     * this method delete the bonus from the playing field.
     * 
     * @param bonusPoint
     *            the position of the bonus that have to be removed.
     * @return a boolean, true if the bonus is remove with success, false if it
     *         isn't
     */
    public final boolean deleteGeneratedBonus(final Point bonusPoint) {
        return this.generatedBonusList.remove(new BonusImpl(bonusPoint));
    }

    /**
     * this method add to the list of bonusGenerationTimers a new
     * BonusGenerationTimer.
     */
    public final void startNewBonusGeneration() {
        this.bonusGenerationTimers.add(new BonusGenerationTimer(Utility.MSEC_CONST,
                Utility.getRandomNumberInRange(1, GameConstant.MAX_TIME_BONUS_GENERATION) * Utility.MSEC_CONST, this,
                this.graphicObs));
    }

    /**
     * this method add to the applied bonus list a bonus passed as parameter.
     * 
     * @param bs
     *            the bonusStrategy object that want to add.
     */
    /*
     * public final void addAppliedBonus(final BonusTimer bs) {
     * this.appliedBonusList.add(bs); }
     */
    /* public final void addAppliedBonus(final ) */
    /**
     * this method delete from the applied bonus list a bonus passed as parameter.
     * 
     * @param bs
     *            the bonusStrategy object that want to delete.
     */
    public final void deleteAppliedBonus(final BonusTimer bs) {
        this.appliedBonusList.remove(bs);
    }

    /**
     * this method delete all the bonus in the applied bonus list.
     */
    public final void deleteAllAppliedBonus() {
        /*
         * instantiates a new list of bonusStrategy from the appliedBonusList, it
         * iterates the new list and then it remove the element from the
         * appliedBonusList.
         */
        List<BonusTimer> copyList = new ArrayList<>(this.appliedBonusList);
        copyList.forEach(x -> {
            x.cancel();
            x.resetStrategy();
            this.deleteAppliedBonus(x);
        });
    }

    /**
     * this method pause all the timers of the bonus in the appliedBonusList.
     */
    public final void pauseAppliedBonus() {
        this.appliedBonusList.forEach(bonus -> bonus.pause());
    }

    /**
     * this method resume all the timers of the bonus in the appliedBonusList.
     */
    public final void resumeAppliedBonus() {
        this.appliedBonusList.forEach(bonus -> bonus.resume());
    }

    /**
     * this method return an unmodifableList of the appliedBonusList.
     * 
     * @return a List unmodifiable of appliedBonusList.
     */
    public final List<BonusTimer> getAppliedBonus() {
        return Collections.unmodifiableList(this.appliedBonusList);
    }

    /**
     * this method resume all the timers in the bonusGenerationTimers.
     */
    public final void resumeBonusGeneratorTimers() {
        this.bonusGenerationTimers.forEach(x -> x.resume());
    }

    /**
     * this method pause all the timers in the bonusGenerationTimers.
     */
    public final void pauseBonusGeneratorTimers() {
        this.bonusGenerationTimers.forEach(x -> x.pause());
    }

    /**
     * this method stop all the timers in the bonusGenerationTimers.
     */
    public final void stopBonusGeneratorTimers() {
        this.bonusGenerationTimers.forEach(x -> x.cancel());
    }

    @Override
    public final void executeOnBonusTimerExpiration(final BonusTimer bs) {
        this.deleteAppliedBonus(bs);
        this.graphicObs.showHitBonusDescription(BonusType.NONE);
    }

}
