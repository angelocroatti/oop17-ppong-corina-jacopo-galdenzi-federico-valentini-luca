package src.model;
/**
 * 
 * @author Federico Galdenzi
 * This interface represent the pattern Strategy for bonus.
 */
public interface BonusStrategy {
    /**
     * This method apply the Strategy bonus.
     */
    void applyStrategy();
    /**
     * This method reset the Strategy bonus.
     */
    void resetStrategy();
    /**
     * This method handle the Strategy bonus.
     */
    void handleStrategy();

}
