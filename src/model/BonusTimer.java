package src.model;

import src.observerpattern.ExpiringBonusTimerObserver;
import src.observerpattern.ObservableExpiringBonusTimer;
/**
 * 
 * @author Federico Galdenzi
 * This abstract class is used to handle the TimerBonus behaviour, when the timer finish then reset the bonus that has to be implemented in the concrete class
 * and at every interval it call handleBehaviour method that has to be implemented in the concrete class.
 */
public abstract class BonusTimer extends Timer implements BonusStrategy, ObservableExpiringBonusTimer {
    private Ball ball;
    private ExpiringBonusTimerObserver observer;
    /**
     * This is the constructor of the bonus timer which you have to pass the parameters.
     * @param ball the ball object
     * @param observer the ExpiringBonusTimerObserver object, in this case the BonusManager
     * @param interval the interval of the timer
     * @param duration the duration of the timer
     */
    protected BonusTimer(final Ball ball, final ExpiringBonusTimerObserver observer, final int interval, final int duration) {
        super(interval, duration);
        this.ball = ball;
        this.observer = observer;
    }
    /**
     * Gets the ball instance.
     * @return the ball which the bonus is applied.
     */
    protected Ball getBall() {
        return this.ball;
    }
    @Override
    protected final void onTick() {
        this.handleStrategy();
    }

    @Override
    protected final void onFinish() {
        this.resetStrategy();
        notifyExpiringBonusTimerObserver();
    }
    @Override
    public final void setExpiringBonusTimerObserver(final ExpiringBonusTimerObserver obs) {
        this.observer = obs;
    }
    @Override
    public final void notifyExpiringBonusTimerObserver() {
        observer.executeOnBonusTimerExpiration(this);
    }
}
