package src.model;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Optional;

import src.utilities.Direction;
/**
 * 
 * @author Federico Galdenzi
 * This class implements the Collision interface.
 * it provides method that handle if the ball it will collide with the border (UP and DOWN) of the playing field.
 */
public class BorderCollision implements CollisionStrategy {
    private Ball ball;
    private Optional<Point> collisionPoint;
    private boolean canMove;
    private static final int CONSTANT_RESET_POSITION = (int) GameConstant.CONSTANT_OF_PROPORTION_Y * 17;
    /**
     * This is the constructor of the BorderCollision class.
     * @param ballModel the ball.
     */
    public BorderCollision(final Ball ballModel) {
        this.ball = ballModel;
    }

    private boolean check() {
        this.canMove = true;
        Rectangle ballBound = this.ball.getShape();
        //I will check if the next move of the ball goes to collision with the border.
        //If there is a collision and the ball is over the border then I set the correctly Y.
        ballBound.setLocation(new Point(this.ball.getProprierty().getX() + this.ball.getProprierty().getSpeedWithSignX(), this.ball.getProprierty().getY() + this.ball.getProprierty().getSpeedWithSignY()));
        if (this.ball.getProprierty().getYDirection() == Direction.DOWN) {
            if (ballBound.intersects(new Rectangle(0, this.ball.getProprierty().getBoundaryWindow().getHeight() - 1, this.ball.getProprierty().getBoundaryWindow().getWidth(), 1 + this.ball.getProprierty().getDiameter()))) {
                if (ballBound.getMaxY() > this.ball.getProprierty().getBoundaryWindow().getYBoundLaneDown()) {
                    this.ball.getProprierty().setY(this.ball.getProprierty().getBoundaryWindow().getYBoundLaneDown() - (this.ball.getProprierty().getDiameter() + CONSTANT_RESET_POSITION));
                    this.canMove = false;
                }
                return true;
            }
        } else if (this.ball.getProprierty().getYDirection() == Direction.UP) {
            if (this.ball.getShape().intersects(new Rectangle(0, -this.ball.getProprierty().getDiameter() + 1, this.ball.getProprierty().getBoundaryWindow().getWidth(), this.ball.getProprierty().getDiameter()))) {
                if (ballBound.getMinY() < this.ball.getProprierty().getBoundaryWindow().getYBoundLaneUp()) {
                   this.ball.getProprierty().setY(this.ball.getProprierty().getBoundaryWindow().getYBoundLaneUp());
                   this.canMove = false;
                }
                return true;
            }
        }
        return false;
    }
 
    @Override
    public final boolean hasCollision() {
        if (check()) {
            this.ball.getProprierty().changeYDirection();
            collisionPoint = Optional.of(new Point(this.ball.getProprierty().getX(), this.ball.getProprierty().getY()));
            return true;
        }
        collisionPoint = Optional.empty();
        this.canMove = true;
        return false;
    }


    @Override
    public final Optional<Point> getCollisionPoint() {
        return this.collisionPoint;
    }

    @Override
    public final boolean canBallMove() {
        return this.canMove;
    }



}
