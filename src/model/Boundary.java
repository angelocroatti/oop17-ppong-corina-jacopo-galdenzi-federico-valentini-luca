package src.model;

import java.awt.Dimension;
/**
 * 
 * @author Federico Galdenzi
 * This class it is used to determine the bound of the playing field.
 * 
 */
public class Boundary {
    private Dimension sizeWindow;
    private int xBoundLaneLeft;
    private int xBoundLaneRight;
    private int yBoundLaneDown;
    private int yBoundLaneUp;
    /**
     * This is the constructor of the class.
     * @param sizeWindow this is the Dimension object of the panel.
     */
    public Boundary(final Dimension sizeWindow) {
        this.sizeWindow = sizeWindow;
        this.xBoundLaneLeft = (int) ((10 / 100) * (int) this.sizeWindow.getWidth());
        this.xBoundLaneRight = (int) ((int) this.sizeWindow.getWidth() - (int) ((10 / 100) * (int) this.sizeWindow.getWidth()));
        this.yBoundLaneDown = (int) this.sizeWindow.getHeight();
        this.yBoundLaneUp = 0;
    }
    /**
     * 
     * @return the value of the left's limit of the playing field.
     */
    public int getXBoundLaneLeft() {
        return this.xBoundLaneLeft;
    }
    /**
     * 
     * @return the value of the right's limit of the playing field.
     */
    public int getXBoundLaneRight() {
        return this.xBoundLaneRight;
    }
    /**
     * 
     * @return the value of the down's limit of the playing field.
     */
    public int getYBoundLaneDown() {
        return this.yBoundLaneDown;
    }
    /**
     * 
     * @return the value of the up's limit of the playing field.
     */
    public int getYBoundLaneUp() {
        return this.yBoundLaneUp;
    }
    /**
     * 
     * @return the height of the panel.
     */
    public int getHeight() {
        return (int) this.sizeWindow.getSize().getHeight();
    }
    /**
     * 
     * @return the width of the panel.
     */
    public int getWidth() {
        return (int) this.sizeWindow.getSize().getWidth();
    }
}
