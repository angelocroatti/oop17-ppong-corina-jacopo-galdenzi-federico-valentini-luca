package src.model;

import java.awt.Toolkit;

/**
 * . This class contains constants about the game
 * 
 * @author Jacopo Corina, Luca Valentini, Federico Galdenzi.
 *
 */
public final class GameConstant {
    /**
     * private instantiates a new game constant.
     */
    private GameConstant() {
    }

    /** The Constant WIDTH. */
    private static final int WIDTH = 1366;
    /** The Constant HEIGHT. */
    private static final int HEIGHT = 768;

    /**
     * Game Loop Sleep Time.
     */
    public static final int GAME_LOOP_SLEEP_TIME = 20;

    /** The Constant CONSTANT_OF_PROPORTION_X. */
    public static final double CONSTANT_OF_PROPORTION_X = (Toolkit.getDefaultToolkit().getScreenSize().getWidth()
            / WIDTH);

    /** The Constant CONSTANT_OF_PROPORTION_Y. */
    public static final double CONSTANT_OF_PROPORTION_Y = (Toolkit.getDefaultToolkit().getScreenSize().getHeight()
            / HEIGHT);

    /** The Constant RACKET_ONE_POSITION_X. */
    public static final int RACKET_ONE_POSITION_X = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() * 2)
            / 100);

    /** The Constant RACKET_TWO_POSITION_X. */
    public static final int RACKET_TWO_POSITION_X = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() * 98)
            / 100);

    /** The Constant RACKET_POSITION_Y. */
    public static final int RACKET_POSITION_Y = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getHeight() * 30)
            / 100);

    /** The Constant RACKET_WIDTH. */
    public static final int RACKET_WIDTH = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 100));

    /** The Constant RACKET_HEIGHT. */
    public static final int RACKET_HEIGHT = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getHeight() * 10)
            / 100);

    /** The Constant VALUE_NOT_SET. */
    public static final int VALUE_NOT_SET = -1;
    /** The Constant DEFAULT_P1_NAME. */
    public static final String DEFAULT_P1_NAME = "P1";
    /** The Constant DEFAULT_P2_NAME. */
    public static final String DEFAULT_P2_NAME = "P2";
    /** The Constant SPEED_BONUS_TIMER_INTERVAL. */
    public static final int SPEED_BONUS_TIMER_INTERVAL = 1;
    /** The Constant SPEED_BONUS_TIMER_DURATION. */
    public static final int SPEED_BONUS_TIMER_DURATION = 5;
    /** The Constant BLINKING_BONUS_TIMER_INTERVAL. */
    public static final double BLINKING_BONUS_TIMER_INTERVAL = 0.1;
    /** The Constant BLINKING_BONUS_TIMER_DURATION. */
    public static final int BLINKING_BONUS_TIMER_DURATION = 2;

    /** The Constant SIZE_BONUS_TIMER_INTERVAL. */
    public static final double SIZE_BONUS_TIMER_INTERVAL = 0.2;

    /** The Constant SIZE_BONUS_TIMER_DURATION. */
    public static final int SIZE_BONUS_TIMER_DURATION = 10;

    /** The Constant MAX_TIME_BONUS_GENERATION. */
    public static final int MAX_TIME_BONUS_GENERATION = 15;

    /** The Constant DEFAULT_MATCH_TIMER_VALUE. */
    public static final int DEFAULT_MATCH_TIMER_VALUE = 60;

    /** The Constant BALL_MAX_SPEED_X. */
    public static final int BALL_MAX_SPEED_X = (int) (10 * CONSTANT_OF_PROPORTION_X);

    /** The Constant BALL_MEDIUM_SPEED_X. */
    public static final int BALL_MEDIUM_SPEED_X = (int) (7 * CONSTANT_OF_PROPORTION_X);

    /** The Constant BALL_MIN_SPEED_X. */
    public static final int BALL_MIN_SPEED_X = (int) (5 * CONSTANT_OF_PROPORTION_X);

    /** The Constant BALL_MIN_SPEED_Y. */
    public static final int BALL_MIN_SPEED_Y = 0;

    /** The Constant BALL_MAX_SPEED_Y. */
    public static final int BALL_MAX_SPEED_Y = (int) (15 * CONSTANT_OF_PROPORTION_Y);

    /** The Constant BONUS_SPEED_X. */
    public static final int BONUS_SPEED_X = (int) (10 * CONSTANT_OF_PROPORTION_X);

    /** The Constant NUMBER_OF_SECTION_BAR. */
    public static final int NUMBER_OF_SECTION_BAR = 3;

    /** The Constant MAX_ANGLE_BALL. */
    public static final int MAX_ANGLE_BALL = (int) (17 * CONSTANT_OF_PROPORTION_X);

    /** The Constant MEDIUM_ANGLE_BALL. */
    public static final int MEDIUM_ANGLE_BALL = (int) (12 * CONSTANT_OF_PROPORTION_X);

    /** The Constant MIN_ANGLE_BALL. */
    public static final int MIN_ANGLE_BALL = (int) (8 * CONSTANT_OF_PROPORTION_X);

    /** The Constant BALL_DIAMETER. */
    public static final int BALL_DIAMETER = (int) (28 * CONSTANT_OF_PROPORTION_X);
    /** The Constant BONUS_DIAMETER. */
    public static final int BONUS_DIAMETER = (int) (28 * CONSTANT_OF_PROPORTION_X);
    /** The Constant SLIDING_CONSTANT. */
    public static final int SLIDING_CONSTANT = 4;

    /** The Constant MAX_BONUS. */
    public static final int MAX_BONUS = 7;

    /** The Constant WAITING_TIME_PLAYER. */
    public static final int WAITING_TIME_PLAYER = 60;
    /** The Constant WAITING_TIME_IA_NORMAL. */
    public static final int WAITING_TIME_IA_NORMAL = GameConstant.WAITING_TIME_PLAYER - 15;

    /** The Constant WAITING_TIME_IA_HARD. */
    public static final int WAITING_TIME_IA_HARD = GameConstant.WAITING_TIME_PLAYER / 2;

    /** The Constant JOPTIONPANE_WIDTH. */
    public static final int JOPTIONPANE_WIDTH = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() * 20)
            / 100);

    /** The Constant JOPTIONPANE_HEIGHT. */
    public static final int JOPTIONPANE_HEIGHT = (int) ((Toolkit.getDefaultToolkit().getScreenSize().getHeight() * 5)
            / 100);
    /**
     * The default match time.
     */
    public static final int DEFAULT_MATCH_TIME = 60;

    /**
     * The minimum match time.
     */
    public static final int MINIMUM_MATCH_TIME = 30;

    /**
     * The maximum match time.
     */
    public static final int MAXIMUM_MATCH_TIME = 300;

    /**
     * The stepsize match time.
     */
    public static final int STEPSIZE_MATCH_TIME = 10;
}
