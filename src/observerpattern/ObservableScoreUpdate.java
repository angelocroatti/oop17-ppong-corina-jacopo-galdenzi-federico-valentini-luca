package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ObservableScoreUpdate {
    /**
     * 
     * @param s the observer which receives match score updates
     */
    void setScoreUpdateObserver(ScoreUpdateObserver s);
    /**
     * 
     * @param playerNumber the number of the player which makes goal
     */
    void notifyScoreUpdateObserver(int playerNumber);
}
