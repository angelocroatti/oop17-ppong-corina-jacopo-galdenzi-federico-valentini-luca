package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ObservableTimer {
    /**
     * 
     * @param observer the observer which receives the match timer value change
     */
    void setTimerObserver(TimerObserver observer);
    /**
     * notifies the observer when a second is elapsed.
     */
    void notifyTimerObserver();
}
