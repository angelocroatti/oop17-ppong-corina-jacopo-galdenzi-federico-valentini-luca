package src.utilities;

import java.util.Random;
/**
 * 
 * @author Federico Galdenzi
 * This enum it represent the type of the bonus.
 * It has a method that returns a random bonus type, but not the last bonus type: "NONE".
 */
public enum BonusType {
    /**
     * This is the SPEED type.
     */
    SPEED,
    /**
     * This is the BLINK type.
     */
    BLINK,
    /**
     * This is the SIZE type.
     */
    SIZE,
    /**
     * This is the NONE type.
     */
    NONE;
    /**
     * this method return a random bonus type.
     * @return a random BonusType object
     */
    public static BonusType getRandomBonusType() {
        Random r = new Random();
        return BonusType.values()[(r.nextInt(BonusType.values().length - 1))];
    }
}
