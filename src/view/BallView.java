package src.view;

import java.awt.geom.Ellipse2D;

/**
 * 
 * @author Federico Galdenzi
 * This class represent the Ball in the MatchZone.
 */
public class BallView {
    private Ellipse2D ball;
    private MatchZone game;

    /**
     * This is the constructor of the BallView.
     * @param matchZone the match zone graphics.
     */
    public BallView(final MatchZone matchZone) {
        this.ball = new Ellipse2D.Double();
        this.game = matchZone;
    }

    /**
     * This method is used to draw the ball in the MatchZone.
     * @param ballX ball x position
     * 
     * @param ballY ball y position
     * 
     * @param diameter the ball diameter
     * 
     */
    public final void drawBall(final int ballX, final int ballY, final int diameter) {
        this.ball.setFrame(ballX, ballY, diameter, diameter);
        game.moveBall();

    }

    /**
     * 
     * @return the graphics instance of the ball
     */
    public final Ellipse2D getInstance() {
        return this.ball;
    }
}
