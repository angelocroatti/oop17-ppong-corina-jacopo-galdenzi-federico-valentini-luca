package src.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.event.ChangeListener;

import src.model.GameConstant;

/**
 * 
 * @author Jacopo Corina
 *
 */

public class OptionsView extends JPanel {
    /**
     * 
     */
    private static final long serialVersionUID = -851835198039769913L;

    /**
     * 
     * @author Jacopo Corina
     * This class represents the graphics part of the options menu
     *
     */

    /**
     * .These are the modifiable elements.
     */
    public enum COLORSTRINGS {
        /**
         * These enum values  indicate game elements.
         */
        Player1Color, Player2Color, BallColor, BackgroundColor;
    }

    private MyJColorChooser chooser;
    private JComboBox<String> cboxColor;
    private JCheckBox soundSwitch;
    private JRadioButton timerSwitch;
    private JSpinner timerValue;
    private JRadioButton maxScoreSwitch;
    private JSpinner maxScoreValue;
    private JButton confirmColor;

    /**
     * this is the constructor method of OptionsView class.
     */
    public OptionsView() {
        JPanel graphicsGroup = new JPanel(new GridLayout(1, 2));
        JPanel secondGroup = new JPanel(new GridLayout(1, 2));
        JPanel colorGroup = new JPanel();
        JPanel soundGroup = new JPanel();
        JPanel matchGroup = new JPanel();
        confirmColor = new JButton("Confirm color");
        confirmColor.setEnabled(false);
        ButtonGroup radioButtonGroup = new ButtonGroup();
        colorGroup.setBorder(BorderFactory.createTitledBorder("Color Options"));
        soundGroup.setBorder(BorderFactory.createTitledBorder("Sound Options"));
        matchGroup.setBorder(BorderFactory.createTitledBorder("Match Options"));
        this.setLayout(new GridLayout(2, 2));
        List<String> list = new LinkedList<>();
        list.add(COLORSTRINGS.Player1Color.toString());
        list.add(COLORSTRINGS.Player2Color.toString());
        list.add(COLORSTRINGS.BallColor.toString());
        list.add(COLORSTRINGS.BackgroundColor.toString());
        chooser = new MyJColorChooser();
        chooser.hideOtherTabs();
        chooser.setEnabled(false);
        chooser.getSelectionModel().addChangeListener(e -> {
            confirmColor.setIcon(getColorIcon(chooser.getSelectionModel().getSelectedColor()));
        });
        cboxColor = new JComboBox<String>();
        list.forEach(e -> cboxColor.addItem(e));
        list.clear();
        cboxColor.addActionListener(e -> {
            chooser.setEnabled(true);
            confirmColor.setEnabled(true);
            confirmColor.setIcon(getColorIcon(chooser.getSelectionModel().getSelectedColor()));

        });

        soundSwitch = new JCheckBox("Enable sound effects", false);

        timerSwitch = new JRadioButton("Timer", true);
        timerValue = new JSpinner(new SpinnerNumberModel(GameConstant.DEFAULT_MATCH_TIME, GameConstant.MINIMUM_MATCH_TIME, GameConstant.MAXIMUM_MATCH_TIME, GameConstant.STEPSIZE_MATCH_TIME));
        timerValue.setEditor(new JSpinner.DefaultEditor(timerValue));
        maxScoreSwitch = new JRadioButton("Maximum score", false);
        maxScoreValue = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
        maxScoreValue.setEditor(new JSpinner.DefaultEditor(maxScoreValue));
        maxScoreValue.setEnabled(false);

        confirmColor.addActionListener(e -> {
            confirmColor.setEnabled(false);
            chooser.setEnabled(false);
        });
        colorGroup.add(cboxColor);
        colorGroup.add(chooser);
        colorGroup.add(confirmColor);
        graphicsGroup.add(colorGroup);
        soundGroup.add(soundSwitch);
        radioButtonGroup.add(timerSwitch);
        radioButtonGroup.add(maxScoreSwitch);
        matchGroup.add(timerSwitch);
        matchGroup.add(timerValue);
        matchGroup.add(maxScoreSwitch);
        matchGroup.add(maxScoreValue);

        secondGroup.add(matchGroup);
        secondGroup.add(soundGroup);
        this.add(graphicsGroup);
        this.add(secondGroup);
    }

    /**
     * 
     * @return timerValue
     */
    public int getTimerValue() {
        return (int) timerValue.getValue();
    }

    /**
     * 
     * @return max score value
     */
    public int getMaxScoreValue() {
        return (int) maxScoreValue.getValue();
    }
    /**
     * 
     * @return the shown element in the checkbox
     */
    public final COLORSTRINGS changingElement() {
        return COLORSTRINGS.valueOf((String) cboxColor.getSelectedItem());
    }
    /**
     * 
     * @param l an action listener
     */
    public final void addColorConfirmListener(final ActionListener l) {
        confirmColor.addActionListener(l);
    }
    /**
     * 
     * @param l an item listener
     */
    public final void addSoundStateChangeListener(final ItemListener l) {
        soundSwitch.addItemListener(l);
    }
    /**
     * 
     * @return the state of sound checkbox
     */
    public final boolean isSoundEnabled() {
        return soundSwitch.isSelected();
    }
    /**
     * 
     * @param l a listener invoked when the  max score radio button is selected
     */
    public final void addMaxScoreSwitchChangeListener(final ItemListener l) {
        maxScoreSwitch.addItemListener(l);
    }
    /**
     * 
     * @param l a listener invoked when the timer radio button is selected
     */
    public final void addTimerSwitchChangeListener(final ItemListener l) {
        timerSwitch.addItemListener(l);
    }
    /**
     * 
     * @param l a listener invoked when the timer spinner value changes
     */
    public final void addTimerValueChangeListener(final ChangeListener l) {
        timerValue.addChangeListener(l);
    }
    /**
     * 
     * @param l a listener invoked when the max score spinner value changes
     */
    public final void addMaxScoreValueChangeListener(final ChangeListener l) {
        maxScoreValue.addChangeListener(l);
    }
    /**
     * 
     */
    public final void disableTimeSpinner() {
        this.timerValue.setEnabled(false);
    }
    /**
     * 
     */
    public final void enableTimeSpinner() {
        this.timerValue.setEnabled(true);
    }
    /**
     * 
     */
    public final void disableScoreSpinner() {
        this.maxScoreValue.setEnabled(false);
    }
    /**
     * 
     */
    public final void enableScoreSpinner() {
        this.maxScoreValue.setEnabled(true);
    }
    /**
     * 
     * @return the choosen color by the user
     */
    public final Color getChoosenColor() {
        return chooser.getSelectionModel().getSelectedColor();
    }

    private class MyJColorChooser extends JColorChooser {
        /**
         * 
         */
        private static final long serialVersionUID = 4190212162650139908L;

        MyJColorChooser() {
            super();
            this.setPreviewPanel(new JPanel());

        }

        public void hideOtherTabs() {
            AbstractColorChooserPanel[] oldPanels = this.getChooserPanels();
            for (int i = 0; i < oldPanels.length; i++) {
                String clsName = oldPanels[i].getClass().getName();
                if (!clsName.equals("javax.swing.colorchooser.DefaultSwatchChooserPanel")) {
                    this.removeChooserPanel(oldPanels[i]);
                }
            }
        }

    }

    private Icon getColorIcon(final Color c) {
        int dimension = 10;
        BufferedImage bImg = new BufferedImage(dimension, dimension, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bImg.createGraphics();
        graphics.setColor(c);
        graphics.fillRect(0, 0, bImg.getWidth(), bImg.getHeight());
        return new ImageIcon(bImg);
    }
}
