package src.view;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
/**
 * 
 * @author Jacopo Corina
 *
 */
public class ScoreZone extends JPanel {
    private static final long serialVersionUID = 3320235583194051295L;
    private JLabel time = new JLabel("", SwingConstants.CENTER);
    private JLabel score = new JLabel("", SwingConstants.CENTER);
    private JLabel hitBonusDescription = new JLabel("", SwingConstants.CENTER);

    /**
     * this is the constructor method of ScoreZone class.
     */
    public ScoreZone() {
        this.setLayout(new GridLayout(1, 3));
        this.add(time);
        this.add(score);
        this.add(hitBonusDescription);
    }

    /**
     * 
     * @return the time that is missing
     */
    public final JLabel getTime() {
        return time;
    }

    /**
     * 
     * @return the score of the match
     */
    public final JLabel getScore() {
        return score;
    }

    /**
     * 
     * @return the description of the bonus taken
     */
    public final JLabel getHitBonusDescription() {
        return hitBonusDescription;
    }
}
