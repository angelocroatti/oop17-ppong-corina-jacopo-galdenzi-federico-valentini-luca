package src.view;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ViewManager {
    /**
     * 
     * @return the main pane of the frame
     */
    JPanel getMainPane();

    /**
     * 
     * @return the menu bar in the frame
     */
    JMenuBar getMenuBar();

    /**
     * 
     * @return the start view
     */
    StartView getStartView();

    /**
     * 
     * @return the game view
     */
    GameView getGameView();

    /**
     * 
     * @return the match zone
     */
    MatchZone getMatchZone();

    /**
     * 
     * @return the statistics view
     */
    StatisticsView getStatisticsView();

    /**
     * 
     * @return the pause view
     */
    PauseView getPauseView();

    /**
     * 
     * @return the options view
     */
    OptionsView getOptionsView();
}
