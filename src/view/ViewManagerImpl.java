package src.view;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableColumn;

import src.controller.GameController;
import src.model.GameConstant;

/**
 * 
 * @author Jacopo Corina
 *
 */
public class ViewManagerImpl implements ViewManager {

    @SuppressWarnings("unused")
    private static final long serialVersionUID = 2936225451510853838L;
    private static final double WIDTH_PERC = 1;
    private static final double HEIGHT_PERC = 0.9;
    private JFrame mainFrame;
    private StartView startView;
    private GameView gameView;
    private StatisticsView statisticsView;
    private OptionsView optionsView;
    private PauseView pauseView;

    /**
     * this is the constructor method of ViewManager class.
     */
    public ViewManagerImpl() {
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        mainFrame = new JFrame("PingPongGame");
        mainFrame.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setResizable(false);
        JMenuBar bar = new JMenuBar();
        bar.add(new JButton("Go back"));
        bar.setVisible(false);
        mainFrame.setJMenuBar(bar);
        JPanel mainPane = this.getMainPane();
        mainPane.setLayout(new CardLayout());
        startView = new StartView();
        startView.setName("STARTVIEW");
        mainPane.add(startView, startView.getName());
        gameView = new GameView();
        gameView.setName("GAMEVIEW");
        mainPane.add(gameView, gameView.getName());
        statisticsView = new StatisticsView();
        statisticsView.setName("STATISTICSVIEW");
        mainPane.add(statisticsView, statisticsView.getName());
        pauseView = new PauseView();
        pauseView.setName("PAUSEVIEW");
        mainPane.add(pauseView, pauseView.getName());
        optionsView = new OptionsView();
        optionsView.setName("OPTIONSVIEW");
        mainPane.add(optionsView, optionsView.getName());
        new GameController(this);
        mainFrame.setVisible(true);
    }

    /**
     * 
     * @param args .
     */

    public static void main(final String[] args) {
        int length = (int) (10 * GameConstant.CONSTANT_OF_PROPORTION_X);
        UIManager.put("ColorChooser.swatchesRecentSwatchSize", new Dimension(length, length));
        UIManager.put("ColorChooser.swatchesSwatchSize", new Dimension(length, length));
        for (final Object key : Collections.list(UIManager.getDefaults().keys())) {
            Object value = UIManager.get(key);
            if (value instanceof Font) {
                final Font desired = changeFontSize((Font) value);
                UIManager.put(key, desired);
            } else if (value instanceof JTable) {
                final int cantCols = ((JTable) value).getColumnCount();
                final JTable table = ((JTable) value);
                final Font desired = changeFontSize(table.getFont());
                TableColumn column;
                table.setFont(desired);
                for (int i = 0; i < cantCols; i++) {
                    column = ((JTable) value).getColumn(i);
                    column.setPreferredWidth((int) (column.getWidth() * GameConstant.CONSTANT_OF_PROPORTION_X));
                }
            } else if (value instanceof JRadioButton) {
                final JRadioButton element = (JRadioButton) value;
                final int width = (int) (element.getSize().getWidth() * GameConstant.CONSTANT_OF_PROPORTION_X);
                final int height = (int) (element.getSize().getHeight() * GameConstant.CONSTANT_OF_PROPORTION_Y);
                element.setPreferredSize(new Dimension(width, height));
            }
        }
        new ViewManagerImpl();

    }

    private static Font changeFontSize(final Font origin) {
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sh = (int) screen.getHeight();
        final float originalSize = origin.getSize();
        final float newSize = originalSize * sh / 800;
        final Font desired = origin.deriveFont(newSize);
        return desired;
    }

    @Override
    public final JPanel getMainPane() {
        return (JPanel) mainFrame.getContentPane();
    }

    @Override
    public final JMenuBar getMenuBar() {
        return this.mainFrame.getJMenuBar();
    }

    @Override
    public final StartView getStartView() {
        return startView;
    }

    @Override
    public final GameView getGameView() {
        return gameView;
    }

    @Override
    public final MatchZone getMatchZone() {
        return gameView.getMatchZone();
    }

    @Override
    public final StatisticsView getStatisticsView() {
        return statisticsView;
    }

    @Override
    public final PauseView getPauseView() {
        return pauseView;
    }

    @Override
    public final OptionsView getOptionsView() {
        return optionsView;
    }

}
